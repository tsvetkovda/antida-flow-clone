import express from 'express';
import bodyParser from 'body-parser';
import { initProject } from './initProject';
import { getConfig } from './utils/getConfig';

const config = getConfig();

const app = express();

app.use(bodyParser.json());

config.projects.forEach((project) => {
  initProject(app, project, config);
});

app.listen(config.server.port, () => {
  console.log(`app listening on port ${config.server.port}`);
});
