function trimControlChars(str) {
  return str.replace(/</gim, '&lt;').replace(/>/gim, '&gt;');
}

export function generateLinkTitle(card) {
  const cardName = trimControlChars(card.name);
  const labelList = card.labels.map(l => `[${l.name}]`).join('');
  const linkTitle = `#${card.idShort}: ${labelList} ${cardName}`;
  const cardLink = `*<${card.url}|${linkTitle}>*`;

  return { linkTitle, cardLink };
}
