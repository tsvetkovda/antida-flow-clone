export function getConfig() {
  const {
    CONFIG_NAME = 'config.json',
  } = process.env;

  /* eslint-disable-next-line global-require, import/no-dynamic-require */
  return require(`../../${CONFIG_NAME}`);
}
