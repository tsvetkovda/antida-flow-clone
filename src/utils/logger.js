import { WebClient } from '@slack/client';
import { getConfig } from './getConfig';

const config = getConfig();

class Logger {
  constructor() {
    this.slackApi = new WebClient(config.logger.slack.token);
    this.slackChannel = config.logger.slack.channel;
  }

  async error(error, place) {
    try {
      await this.slackApi.chat.postMessage({
        channel: this.slackChannel,
        text: `Error in ${config.stateIdentifier}, ${place}\n\`\`\`${JSON.stringify((error.response && error.response.data) || error.error || error.errors || error.message)}\`\`\``,
      });
    } catch (e) {
      console.error(e);
    }
  }

  async info(message) {
    try {
      await this.slackApi.chat.postMessage({
        channel: this.slackChannel,
        text: `Info message\n> ${message}`,
      });
    } catch (e) {
      console.error(e);
    }
  }
}

export const logger = new Logger();
