import fs from 'fs';
import { getConfig } from './getConfig';
import { logger } from './logger';

const config = getConfig();

export class FileSystem {
  constructor(projectName) {
    this.filename = `${config.pathToStorageFile}${projectName}_storage.json`;
    try {
      this.storage = JSON.parse(fs.readFileSync(this.filename));
    } catch (err) {
      this.storage = {
        tasks: [],
        pullRequests: [],
        usersPullRequests: [],
      };
      fs.writeFileSync(this.filename, JSON.stringify(this.storage));
      logger.info(`Не удалось найти или прочитать существующий файл данных. Поэтому был создан новый ${this.filename}`);
    }
  }

  setValue(type, value) {
    try {
      if (!this.storage[type]) {
        this.storage[type] = [];
      }

      this.storage[type].push(value);
      fs.writeFileSync(this.filename, JSON.stringify(this.storage));
    } catch (e) {
      logger.error(e, 'fileUpdateHandler');
    }
  }

  getValue(type, id) {
    const filtered = this.storage[type].filter(item => item.id === id);
    if (filtered.length > 0) {
      return filtered[0];
    }

    return undefined;
  }

  getValueByField(type, field, fieldValue) {
    const filtered = this.storage[type].filter(item => item[field] === fieldValue);

    if (filtered.length > 0) {
      return filtered[0];
    }

    return undefined;
  }

  getList(type) {
    return this.storage[type];
  }

  deleteValue(type, id) {
    try {
      this.storage[type] = this.storage[type].filter(item => item.id !== id);
      fs.writeFileSync(this.filename, JSON.stringify(this.storage));
    } catch (e) {
      logger.error(e, 'fileUpdateHandler DeleteMethod');
    }
  }
}
