export function getBuildIdText({ number, url, jobName }) {
  return `*<http://${url}/job/${jobName}/${number}/console|Сборка #${number}>*`;
}
