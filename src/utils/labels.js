export function hasLabel(labelList, label) {
  return labelList.some(({ name }) => name.toLowerCase().includes(label));
}
