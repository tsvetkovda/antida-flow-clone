import { WebClient } from '@slack/client';
import jenkins from 'jenkins';

import { approveHandler } from './handlers/approve';
import { boardUpdateHandler } from './handlers/boardUpdate';
import { buildCompleteHandler } from './handlers/buildComplete';
import { prodBuildCompleteHandler } from './handlers/prodBuildComplete';
import { pullRequestMergedHandler } from './handlers/pullRequestMerged';
import { pullRequestDeclinedHandler } from './handlers/pullRequestDeclined';
import { pullRequestReviewerHandler } from './handlers/pullRequestReviewerCheck';
import { commentHandler } from './handlers/comment';
import { TrelloApi } from './api/trello';
import { BitbucketApi } from './api/bitbucket';
import { FileSystem } from './utils/fileSystem';

async function initTrelloHooks(project, trelloApi) {
  trelloApi.createBoardUpdateHook(project);
}

export async function initProject(app, project, config) {
  const {
    appHostName,
    trelloAuth,
    slackAuth,
    bitbucketAuth,
    robotTrelloUserName,
  } = config;

  const apis = {
    trelloApi: new TrelloApi(trelloAuth.key, trelloAuth.token, appHostName),
    slackApi: new WebClient(slackAuth.token),
    bitbucketApi: new BitbucketApi(
      bitbucketAuth.key,
      bitbucketAuth.secret,
      project.bitbucket.repositoryName,
      project.bitbucket.repositoryAuthor,
      project.productionBranchName,
      project.devServerBranchName,
    ),
    filesystem: new FileSystem(project.name),
  };

  const trelloBoard = await apis.trelloApi.getBoard(project.board.id);
  if (trelloBoard) {
    // eslint-disable-next-line no-param-reassign
    project.board.id = trelloBoard.id;
  }

  if (project.jenkinsBuildDev) {
    const jenkinsParams = { ...project.jenkinsBuildDev };
    const { protocol = 'http' } = jenkinsParams;
    const baseUrl = `${protocol}://${jenkinsParams.userName}:${jenkinsParams.userToken}@${jenkinsParams.url}/`;
    apis.jenkinsAPI = jenkins({
      baseUrl,
      promisify: true,
    });
    apis.jenkinsAPI.jenkinsParams = jenkinsParams;
  }

  app.post(`/${project.name}/hooks/pr-approved/`, approveHandler(project, apis));
  app.post(`/${project.name}/hooks/pr-commented/`, commentHandler(project, apis));
  app.post(`/${project.name}/hooks/board-update/`, boardUpdateHandler(project, apis, robotTrelloUserName));
  app.post(`/${project.name}/hooks/build-finished/`, buildCompleteHandler(project, apis));
  app.post(`/${project.name}/hooks/prod-build-finished/`, prodBuildCompleteHandler(project, apis));
  app.post(`/${project.name}/hooks/pr-merged/`, pullRequestMergedHandler(project, apis));
  app.post(`/${project.name}/hooks/pr-declined/`, pullRequestDeclinedHandler(apis));
  app.post(`/${project.name}/hooks/pr-reviewer-check/`, pullRequestReviewerHandler(project, apis));

  app.head(`/${project.name}/hooks/board-update/`, (req, res) => res.sendStatus(200));

  initTrelloHooks(project, apis.trelloApi, appHostName);
}
