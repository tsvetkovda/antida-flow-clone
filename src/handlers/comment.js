import { logger } from '../utils/logger';
import { generateLinkTitle } from '../utils/generateLinkTitle';

const DECLINE_COMMENT = '#decline';

export function commentHandler(project, apis) {
  const { board, slack } = project;
  const { trelloApi, slackApi, filesystem } = apis;

  return async (req, res) => {
    try {
      const branchName = req.body.pullrequest.source.branch.name;
      const pullrequestId = req.body.pullrequest.id;
      const pullrequestLink = req.body.pullrequest.links.html.href;
      const comment = req.body.comment.content.raw;
      const commentAuthor = req.body.comment.user.account_id;
      const cardNumber = parseInt(branchName, 10);

      const [boardLists, cards] = await Promise.all([
        trelloApi.getBoardLists(board.id),
        trelloApi.getBoardCards(board.id),
      ]);

      const reopenedList = boardLists.find(l => l.name === board.reopenedStatus);
      const inReviewList = boardLists.find(l => l.name === board.inReviewStatus);
      const card = cards.find(c => c.idShort === cardNumber);
      const { linkTitle, cardLink } = generateLinkTitle(card);

      if (comment.includes(DECLINE_COMMENT)) {
        if (card.idList === inReviewList.id) {
          await trelloApi.setCardList(card.id, reopenedList.id);

          await slackApi.chat.postMessage({
            channel: slack.channel,
            text: `${cardLink} \n> пулл-реквест отклонен \n> задача переведена в статус _${board.reopenedStatus}_`,
          });
        } else {
          await slackApi.chat.postMessage({
            channel: slack.channel,
            text: `${cardLink} \n> не переведена в статус _${board.reopenedStatus}_ \n> потому что не в статусе _${board.inReviewStatus}_`,
          });
        }
      } else if (project.users) {
        const slackUserInfo = filesystem.getValue('usersPullRequests', pullrequestId);
        if (slackUserInfo && slackUserInfo.slackId) {
          const prAuthor = project.users.filter(user => user.slackUserId === slackUserInfo.slackId);
          const prAuthorFinded = prAuthor && prAuthor.length > 0;

          if (commentAuthor && prAuthorFinded && prAuthor[0].bitbucketAccount === commentAuthor) {
            return;
          }

          const slackChannel = await slackApi.conversations.open({
            users: slackUserInfo.slackId,
          });
          await slackApi.chat.postMessage({
            channel: slackChannel.channel.id,
            text: `*<${pullrequestLink}|${linkTitle}>* \n> Новый комментарий: \n> ${comment}`,
          });
        } else {
          await slackApi.chat.postMessage({
            channel: slack.channel,
            text: `*<${pullrequestLink}|${linkTitle}>* \n> В Pull Request добавлен комментарий, однако автора PR найти не удалось.`,
          });
          throw new Error(`Не удаётся найти пользователя, создававшего PR, ${slackUserInfo.slackId}`);
        }
      } else {
        await slackApi.chat.postMessage({
          channel: slack.channel,
          text: `*<${pullrequestLink}|${linkTitle}>* \n> В Pull Request добавлен комментарий, однако автора PR найти не удалось.`,
        });
      }

      res.sendStatus(200);
    } catch (e) {
      logger.error(e, 'commentHandler');
      res.sendStatus(200);
    }
  };
}
