import { logger } from '../utils/logger';
import { generateLinkTitle } from '../utils/generateLinkTitle';

export function pullRequestMergedHandler(project, apis) {
  const { board, productionBranchName } = project;
  const {
    slackApi,
    bitbucketApi,
    filesystem,
    trelloApi,
  } = apis;

  return async (req, res) => {
    try {
      const pullrequestId = req.body.pullrequest.id;
      const destinationBranch = req.body.pullrequest.destination.branch.name;

      if (pullrequestId && destinationBranch === productionBranchName) {
        const cardObject = await filesystem.getValueByField('pullRequests', 'pullRequestId', pullrequestId);

        if (cardObject) {
          await filesystem.deleteValue('pullRequests', cardObject.id);
        }

        await filesystem.deleteValue('usersPullRequests', pullrequestId);

        const pullRequests = filesystem.getList('usersPullRequests');

        if (pullRequests && pullRequests.length > 0) {
          pullRequests.forEach(async (element) => {
            const isConflicted = await bitbucketApi.checkPullrequestConflict(element.id);

            if (isConflicted) {
              const conflictedPullRequest = await bitbucketApi.getPullRequestInfo(element.id);

              const [boardLists, cards] = await Promise.all([
                trelloApi.getBoardLists(board.id),
                trelloApi.getBoardCards(board.id),
              ]);
              const branchName = conflictedPullRequest.source.branch.name;
              const cardNumber = parseInt(branchName, 10);
              const reopenedList = boardLists.find(l => l.name === board.reopenedStatus);
              const card = cards.find(c => c.idShort === cardNumber);
              const { linkTitle, cardLink } = generateLinkTitle(card);

              if (card.idList !== reopenedList.id) {
                await trelloApi.setCardList(card.id, reopenedList.id);
              }

              const slackChannel = await slackApi.conversations.open({
                users: element.slackId,
              });
              await slackApi.chat.postMessage({
                channel: slackChannel.channel.id,
                text: `*<${conflictedPullRequest.links.html.href}|${linkTitle}>* \n> Возник конфликт в твоём пулл-реквесте. \n> ${cardLink} \n> Карточка перенесена в _${board.reopenedStatus}_.`,
              });
            }
          });
        }
      }

      res.sendStatus(200);
    } catch (e) {
      logger.error(e, 'pullRequestMergedHandler');
      res.sendStatus(200);
    }
  };
}
