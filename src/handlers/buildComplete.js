import { logger } from '../utils/logger';
import { getBuildIdText } from '../utils/formatMessage';

export function buildCompleteHandler(project, apis) {
  const { board, slack, jenkinsBuildDev } = project;
  const { slackApi, trelloApi, filesystem } = apis;

  return async (req, res) => {
    try {
      const { status, number } = req.body.build;
      const queueNumber = req.body.build.queue_id;
      const cardInfo = filesystem.getValue('tasks', queueNumber);
      const { cardNumber, cardLink } = cardInfo;
      const buildText = getBuildIdText({ ...jenkinsBuildDev, number });

      if (status === 'SUCCESS') {
        if (cardInfo) {
          const [boardLists, cards] = await Promise.all([
            trelloApi.getBoardLists(board.id),
            trelloApi.getBoardCards(board.id),
          ]);
          const card = cards.find(c => c.idShort === cardNumber);
          const stagingList = boardLists.find(l => l.name === board.stagingStatus);
          const readyForTestingList = boardLists.find(l => l.name === board.readyForTestingStatus);

          if (card.idList === stagingList.id) {
            await trelloApi.setCardList(card.id, readyForTestingList.id);

            await slackApi.chat.postMessage({
              channel: slack.channel,
              text: `${cardLink} \n> Задача развёрнута на dev сервер (${buildText}) и переведена в статус _${board.readyForTestingStatus}_`,
            });
          } else {
            await slackApi.chat.postMessage({
              channel: slack.channel,
              text: `${cardLink} \n> Задача развёрнута на dev сервер (${buildText}), но не переведена в статус _${board.readyForTestingStatus}_ \n> потому что не в статусе _${board.stagingStatus}_`,
            });
          }
        } else {
          await slackApi.chat.postMessage({
            channel: slack.channel,
            text: `${buildText} dev сервера завершена. Кажется, для этой сборки нет соответствующей карточки.`,
          });
        }
      } else {
        await slackApi.chat.postMessage({
          channel: slack.channel,
          text: `${buildText} dev сервера завершилась ошибкой или была отменена вручную.`,
        });
      }

      filesystem.deleteValue('tasks', queueNumber);
      res.sendStatus(200);
    } catch (e) {
      logger.error(e, 'buildCompleteHandler');
      const queueNumber = req.body.build.queue_id;
      filesystem.deleteValue('tasks', queueNumber);
      res.sendStatus(200);
    }
  };
}
