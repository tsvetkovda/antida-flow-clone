import { logger } from '../utils/logger';

export function prodBuildCompleteHandler(project, apis) {
  const { board, slack } = project;
  const { slackApi, trelloApi } = apis;

  return async (req, res) => {
    try {
      const { status } = req.body.build;

      if (status === 'SUCCESS') {
        const [boardLists, cards] = await Promise.all([
          trelloApi.getBoardLists(board.id),
          trelloApi.getBoardCards(board.id),
        ]);

        const masterList = boardLists.find(l => l.name === board.inMasterStatus);
        const productionList = boardLists.find(l => l.name === board.inProductionStatus);
        const cardsInMaster = cards.filter(c => c.idList === masterList.id);
        let cardLinks = '';

        cardsInMaster.forEach(async (card) => {
          cardLinks = cardLinks + `<${card.url}|${card.idShort}> `;
        });

        cardsInMaster.forEach(async (card) => {
          await trelloApi.setCardList(card.id, productionList.id);
        });

        if (project.testers) {
          let testersString = '';

          project.testers.forEach((tester) => {
            testersString += `<@${tester.slackUserId}>, `;
          });

          await slackApi.chat.postMessage({
            channel: slack.channel,
            text: `Завершена сборка production сервера. Задачи ${cardLinks} перенесены в статус ${board.inProductionStatus}.`,
          });

          await slackApi.chat.postMessage({
            channel: slack.channel,
            text: `${testersString}необходимо выполнить смоук-тестирование.`,
          });
        }
      } else {
        await slackApi.chat.postMessage({
          channel: slack.channel,
          text: 'Cборка production сервера завершилась ошибкой или была отменена вручную.',
        });
      }

      res.sendStatus(200);
    } catch (e) {
      logger.error(e, 'prodBuildCompleteHandler');
      res.sendStatus(200);
    }
  };
}
