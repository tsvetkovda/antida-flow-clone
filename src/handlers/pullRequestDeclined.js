import { logger } from '../utils/logger';

export function pullRequestDeclinedHandler(apis) {
  const {
    filesystem,
  } = apis;

  return async (req, res) => {
    try {
      const pullrequestId = req.body.pullrequest.id;

      if (pullrequestId) {
        const cardObject = await filesystem.getValueByField('pullRequests', 'pullRequestId', pullrequestId);

        if (cardObject) {
          await filesystem.deleteValue('pullRequests', cardObject.id);
        }

        await filesystem.deleteValue('usersPullRequests', pullrequestId);
      }

      res.sendStatus(200);
    } catch (e) {
      logger.error(e, 'pullRequestDeclinedHandler');
      res.sendStatus(200);
    }
  };
}
