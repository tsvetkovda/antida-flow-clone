import { logger } from '../utils/logger';
import { hasLabel } from '../utils/labels';
import { generateLinkTitle } from '../utils/generateLinkTitle';

export function boardUpdateHandler(project, apis, robotTrelloUserName) {
  const {
    board,
    slack,
    productionBranchName,
    devServerBranchName,
  } = project;
  const {
    slackApi,
    bitbucketApi,
    trelloApi,
    filesystem,
    jenkinsAPI,
  } = apis;

  return async (req, res) => {
    try {
      const {
        action: {
          type,
          data: { listAfter, listBefore, card },
          memberCreator,
        },
      } = req.body;

      const trelloUser = memberCreator.username;

      const hasListsInfo = listBefore && listAfter;
      if (hasListsInfo && type === 'updateCard' && project.board.label) {
        const cards = await trelloApi.getBoardCards(board.id);
        const fullCard = cards.find(c => c.idShort === card.idShort);
        if (!hasLabel(fullCard.labels, project.board.label)) {
          res.sendStatus(200);

          return;
        }
      }

      //  перевод карточки в Code Review
      if (hasListsInfo && type === 'updateCard' && listBefore.name !== board.inReviewStatus && listAfter.name === board.inReviewStatus) {
        const branches = await bitbucketApi.getBranches(card.idShort);
        const currentBranch = branches.find((b) => {
          const regex = new RegExp(`^${card.idShort}(\\D|$)`);

          return b.name.match(regex) !== null;
        });

        const cards = await trelloApi.getBoardCards(board.id);
        const fullCard = cards.find(c => c.idShort === card.idShort);
        const { linkTitle } = generateLinkTitle(fullCard);
        if (!currentBranch) {
          throw new Error(`boardUpdateHandler, перенесли карточку в статус ${board.inReviewStatus}. Нет ветки с id ${card.idShort} (updateCard currentBranch)`);
        }

        let prAuthorBitbucketAccount;
        let userInfo;
        if (project.users) {
          userInfo = project.users.filter(user => user.trelloUserName === trelloUser);
          if (userInfo && userInfo.length > 0) {
            prAuthorBitbucketAccount = userInfo[0].bitbucketAccount;
          } else {
            throw new Error('Не удалось создать PR, так как автор не найден в конфигурации');
          }
        }

        const response = await bitbucketApi.createPullrequest(currentBranch.name, `${card.idShort}: ${card.name}`, prAuthorBitbucketAccount);
        const { id, links } = response || {};

        if (id && userInfo) {
          const prRecord = filesystem.getValue('usersPullRequests', id);

          if (userInfo.length > 0) {
            if (!prRecord) {
              filesystem.setValue('usersPullRequests', {
                id,
                slackId: userInfo[0].slackUserId,
              });
            }
          } else {
            logger.error(new Error('При создании PR не удалось сопоставить его с пользователем системы, так как таковой не был найден в конфигурации'), 'userChannelSearch');
          }
        }

        if (id && card.idShort) {
          const prRecord = filesystem.getValue('pullRequests', card.idShort);

          if (!prRecord) {
            filesystem.setValue('pullRequests', {
              id: card.idShort,
              pullRequestId: id,
            });
          }
        }

        await slackApi.chat.postMessage({
          channel: slack.channel,
          text: `*<${links.html.href}|${linkTitle}>* \n> ${memberCreator.fullName || memberCreator.username} перевел задачу в статус _${board.inReviewStatus}_`,
        });
      }

      // Перевод карточки в Ready For Staging
      if (hasListsInfo && type === 'updateCard' && listBefore.name !== board.stagingStatus && listAfter.name === board.stagingStatus) {
        if (card && card.idShort) {
          const afterApproveCard = trelloUser === robotTrelloUserName;

          if (!afterApproveCard) {
            const pullRequest = filesystem.getValue('pullRequests', card.idShort);
            const prInfo = await bitbucketApi.getPullRequestInfo(pullRequest.pullRequestId);
            const branchName = prInfo.source.branch.name;
            const cardNumber = parseInt(branchName, 10);
            const cards = await trelloApi.getBoardCards(board.id);
            const fullCard = cards.find(c => c.idShort === card.idShort);
            const { linkTitle, cardLink } = generateLinkTitle(fullCard);

            if (prInfo && prInfo.participants) {
              const approvers = prInfo.participants.filter(p => p.approved === true);

              if (approvers && approvers.length > 0) {
                await slackApi.chat.postMessage({
                  channel: slack.channel,
                  text: `${cardLink} \n> Задача переведена в статус _${board.stagingStatus}_ \n> Начата подготовка к развёртыванию на dev сервер.`,
                });

                const devPullrequest = await bitbucketApi.createPullrequest(branchName, `*flowbot to ${devServerBranchName}*: ${card.idShort} - ${card.name}`, undefined, devServerBranchName);
                const devPullrequestId = devPullrequest && devPullrequest.id;

                if (devPullrequestId) {
                  const mergeResult = await bitbucketApi.mergePullrequest(devPullrequestId, card.idShort);

                  if (mergeResult) {
                    await slackApi.chat.postMessage({
                      channel: slack.channel,
                      text: `${cardLink} \n> Готова к развёртыванию на dev сервер`,
                    });

                    if (jenkinsAPI) {
                      const queueNumber = await jenkinsAPI.job.build(
                        {
                          name: jenkinsAPI.jenkinsParams.jobName,
                          token: jenkinsAPI.jenkinsParams.jobToken,
                        },
                      );

                      filesystem.setValue('tasks', {
                        id: queueNumber,
                        cardNumber,
                        cardLink,
                      });

                      await slackApi.chat.postMessage({
                        channel: slack.channel,
                        text: `${cardLink} \n> Разворачивается на dev сервер. ID сборки ${queueNumber}`,
                      });
                    }
                  } else {
                    await slackApi.chat.postMessage({
                      channel: slack.channel,
                      text: `*<${devPullrequest.links.html.href}|${linkTitle}>* \n> Произошла ошибка при попытке смержить ветку в ${devServerBranchName}, возможно, конфликт`,
                    });
                  }
                } else if (jenkinsAPI) {
                  const queueNumber = await jenkinsAPI.job.build(
                    {
                      name: jenkinsAPI.jenkinsParams.jobName,
                      token: jenkinsAPI.jenkinsParams.jobToken,
                    },
                  );

                  filesystem.setValue('tasks', {
                    id: queueNumber,
                    cardNumber,
                    cardLink,
                  });

                  await slackApi.chat.postMessage({
                    channel: slack.channel,
                    text: `${cardLink} \n> Разворачивается на dev сервер. ID сборки ${queueNumber}`,
                  });
                }
              } else {
                await slackApi.chat.postMessage({
                  channel: slack.channel,
                  text: `${cardLink} \n> Задача переведена в статус _${board.stagingStatus}_ \n> Соответствующий PullRequest ещё не получил одобрение \n> Развертывание отменено.`,
                });
              }
            }
          }
        }
      }

      if (hasListsInfo && type === 'updateCard' && listBefore.name !== board.doneStatus && listAfter.name === board.doneStatus) {
        const pullRequestInfo = filesystem.getValue('pullRequests', card.idShort);

        if (!pullRequestInfo) {
          throw new Error(`boardUpdateHandler, перенесли карточку в статус ${board.doneStatus}. Не найден соответствующий Pull Request`);
        }

        const mergeResult = await bitbucketApi.mergePullrequest(pullRequestInfo.pullRequestId);

        if (mergeResult) {
          const [boardLists, cards] = await Promise.all([
            trelloApi.getBoardLists(board.id),
            trelloApi.getBoardCards(board.id),
          ]);
          const fullCard = cards.find(c => c.idShort === card.idShort);
          const { cardLink } = generateLinkTitle(fullCard);
          const doneList = boardLists.find(l => l.name === board.doneStatus);
          const inMasterList = boardLists.find(l => l.name === board.inMasterStatus);

          if (card.idList === doneList.id) {
            await trelloApi.setCardList(card.id, inMasterList.id);

            await slackApi.chat.postMessage({
              channel: slack.channel,
              text: `${cardLink} \n> Тестирование задачи завершено. \n> Соответствующий Pull Request залит в ${productionBranchName}, карточка перемещена в статус _${board.inMasterStatus}_`,
            });
          } else {
            await slackApi.chat.postMessage({
              channel: slack.channel,
              text: `${cardLink} \n> не переведена в статус _${board.inMasterList}_ \n> потому что не в статусе _${board.doneList}_`,
            });
          }
        } else {
          const cards = await trelloApi.getBoardCards(board.id);
          const fullCard = cards.find(c => c.idShort === card.idShort);
          const { cardLink } = generateLinkTitle(fullCard);

          await slackApi.chat.postMessage({
            channel: slack.channel,
            text: `${cardLink} \n> Произошла ошибка при попытке смержить ветку в ${productionBranchName}, возможно, конфликт \n> Необходимо ручное решение проблем`,
          });

          if (project.users) {
            const slackUserInfo = filesystem.getValue('usersPullRequests', pullRequestInfo.pullRequestId);
            if (slackUserInfo && slackUserInfo.slackId) {
              const slackChannel = await slackApi.conversations.open({
                users: slackUserInfo.slackId,
              });
              await slackApi.chat.postMessage({
                channel: slackChannel.channel.id,
                text: `${cardLink} \n> Произошла ошибка при попытке смержить ветку в ${productionBranchName}, возможно, конфликт \n> Необходимо ручное решение проблем`,
              });
            } else {
              await slackApi.chat.postMessage({
                channel: slack.channel,
                text: `${cardLink} \n> Произошла ошибка при попытке смержить ветку в ${productionBranchName}, возможно, конфликт \n> Необходимо ручное решение проблем, однако автор не найден`,
              });
              throw new Error(`Не удаётся найти пользователя, создававшего PR, ${slackUserInfo.slackId}`);
            }
          } else {
            await slackApi.chat.postMessage({
              channel: slack.channel,
              text: `${cardLink} \n> Произошла ошибка при попытке смержить ветку в ${productionBranchName}, возможно, конфликт \n> Необходимо ручное решение проблем, однако автор не найден`,
            });
          }
        }

        filesystem.deleteValue('pullRequests', card.idShort);
        filesystem.deleteValue('usersPullRequests', pullRequestInfo.pullRequestId);
      }

      res.sendStatus(200);
    } catch (e) {
      logger.error(e, 'boardUpdateHandler');
      res.sendStatus(200);
    }
  };
}
