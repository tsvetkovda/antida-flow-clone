import { logger } from '../utils/logger';

export function pullRequestReviewerHandler(project, apis) {
  const {
    slackApi,
    filesystem,
  } = apis;
  const {
    users,
    slack,
  } = project;

  return async (req, res) => {
    try {
      const {
        id,
        reviewers,
        links,
        title,
      } = req.body.pullrequest;

      if (reviewers && reviewers.length > 0 && users) {
        reviewers.forEach(async (reviewer) => {
          const userInfo = users.filter(user => user.bitbucketAccount === reviewer.account_id);
          const prAuthor = filesystem.getValue('usersPullRequests', id);

          if (userInfo && userInfo.length > 0) {
            const isAuthor = prAuthor.slackId === userInfo[0].slackUserId;
            const message = isAuthor
              ? `*<${links.html.href}|#${title}>* \n> Пулл-реквест по твоей задаче был создан или обновлён.`
              : `*<${links.html.href}|#${title}>* \n> Вы были назначены для проведения ревью этого пулл-реквеста.`;
            const slackChannel = await slackApi.conversations.open({
              users: userInfo[0].slackUserId,
            });
            await slackApi.chat.postMessage({
              channel: slackChannel.channel.id,
              text: message,
            });
          } else {
            await slackApi.chat.postMessage({
              channel: slack.channel,
              text: `*<${links.html.href}|#${title}>* \n> Пользователь ${reviewer.nickname} был назначен для ревью этого пулл-реквеста, но запись о нём не найдена в списке участников проекта.`,
            });
          }
        });
      }

      res.sendStatus(200);
    } catch (e) {
      logger.error(e, 'pullRequestReviewerHandler');
      res.sendStatus(200);
    }
  };
}
