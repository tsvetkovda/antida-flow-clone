import { logger } from '../utils/logger';
import { generateLinkTitle } from '../utils/generateLinkTitle';

export function approveHandler(project, apis) {
  const {
    board,
    slack,
    devServerBranchName,
  } = project;
  const {
    trelloApi,
    slackApi,
    bitbucketApi,
    jenkinsAPI,
    filesystem,
  } = apis;

  return async (req, res) => {
    try {
      const branchName = req.body.pullrequest.source.branch.name;
      const cardNumber = parseInt(branchName, 10);

      const [boardLists, cards] = await Promise.all([
        trelloApi.getBoardLists(board.id),
        trelloApi.getBoardCards(board.id),
      ]);

      const stagingList = boardLists.find(l => l.name === board.stagingStatus);
      const inReviewList = boardLists.find(l => l.name === board.inReviewStatus);
      const card = cards.find(c => c.idShort === cardNumber);
      const { linkTitle, cardLink } = generateLinkTitle(card);

      if (card.idList === inReviewList.id) {
        await trelloApi.setCardList(card.id, stagingList.id);

        await slackApi.chat.postMessage({
          channel: slack.channel,
          text: `${cardLink} \n> Code Review завершено успешно \n> задача переведена в статус _${board.stagingStatus}_`,
        });
      } else {
        await slackApi.chat.postMessage({
          channel: slack.channel,
          text: `${cardLink} \n> не переведена в статус _${board.stagingStatus}_ \n> потому что не в статусе _${board.inReviewStatus}_`,
        });
      }

      const { id, links } = await bitbucketApi.createPullrequest(branchName, `*flowbot to ${devServerBranchName}*: ${card.idShort} - ${card.name}`, undefined, devServerBranchName);

      if (id) {
        const mergeResult = await bitbucketApi.mergePullrequest(id, card.idShort);

        if (mergeResult) {
          await slackApi.chat.postMessage({
            channel: slack.channel,
            text: `${cardLink} \n> Готова к развёртыванию на dev сервер`,
          });

          if (jenkinsAPI) {
            const queueNumber = await jenkinsAPI.job.build(
              {
                name: jenkinsAPI.jenkinsParams.jobName,
                token: jenkinsAPI.jenkinsParams.jobToken,
              },
            );

            filesystem.setValue('tasks', {
              id: queueNumber,
              cardNumber,
              cardLink,
            });

            await slackApi.chat.postMessage({
              channel: slack.channel,
              text: `${cardLink} \n> Разворачивается на dev сервер. ID сборки ${queueNumber}`,
            });
          }
        } else {
          await slackApi.chat.postMessage({
            channel: slack.channel,
            text: `*<${links.html.href}|${linkTitle}>* \n> Произошла ошибка при попытке смержить ветку в ${devServerBranchName}, возможно, конфликт`,
          });
        }
      }

      res.sendStatus(200);
    } catch (e) {
      logger.error(e, 'approveHandler');
      res.sendStatus(200);
    }
  };
}
