import Axios from 'axios';
import { stringify } from 'querystring';
import { logger } from '../utils/logger';

export class BitbucketApi {
  constructor(
    key, secret, repositoryName, repositoryAuthor, productionBranchName, devServerBranchName,
  ) {
    this.repositoryName = repositoryName;
    this.repositoryAuthor = repositoryAuthor;
    this.productionBranchName = productionBranchName;
    this.devServerBranchName = devServerBranchName;
    this.getToken(key, secret);
  }

  get baseApiUrl() {
    return `https://api.bitbucket.org/2.0/repositories/${this.repositoryAuthor}/${this.repositoryName}`;
  }

  async getToken(key, secret) {
    try {
      const { data } = await Axios.post('https://bitbucket.org/site/oauth2/access_token', stringify({ grant_type: 'client_credentials' }), {
        auth: {
          username: key,
          password: secret,
        },
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      });

      this.accessToken = data.access_token;
      setTimeout(() => {
        this.getToken(key, secret);
      }, data.expires_in * 0.9 * 1000);
    } catch (e) {
      logger.error(e, 'getToken');

      setTimeout(() => {
        this.getToken(key, secret);
      }, 5000);
    }
  }

  async createPullrequest(branchName, title, author, destinationBranchName) {
    try {
      let reviewers;
      if (author) {
        reviewers = [{}];
        reviewers[0].account_id = author;
      }
      const closeSourceBranch = false;
      const { data } = await Axios.post(
        `${this.baseApiUrl}/pullrequests`,
        {
          source: {
            branch: {
              name: branchName,
            },
          },
          destination: {
            branch: {
              name: destinationBranchName || this.productionBranchName,
            },
          },
          title,
          close_source_branch: closeSourceBranch,
          reviewers,
        },
        { headers: { Authorization: `Bearer ${this.accessToken}` } },
      );

      return data;
    } catch (e) {
      logger.error(e, 'createPullrequest');

      return undefined;
    }
  }

  async mergePullrequest(pullrequestId, cardId) {
    try {
      const { data } = await Axios.post(
        `${this.baseApiUrl}/pullrequests/${pullrequestId}/merge`,
        {
          message: cardId ? `Task ${cardId} automatically merged to ${this.devServerBranchName} after approve` : `Pull Request ${pullrequestId} automatically merged to ${this.productionBranchName} after testing`,
          close_source_branch: false,
        },
        { headers: { Authorization: `Bearer ${this.accessToken}` } },
      );

      return data;
    } catch (e) {
      logger.error(e, 'mergePullRequest');

      return undefined;
    }
  }

  async checkPullrequestConflict(pullrequestId) {
    try {
      const { data } = await Axios.get(
        `${this.baseApiUrl}/pullrequests/${pullrequestId}/diff`,
        {
          headers: { Authorization: `Bearer ${this.accessToken}` },
        },
      );

      return data.includes('<<<<<<<');
    } catch (e) {
      logger.error(e, 'checkPullrequestConflict');

      return false;
    }
  }

  async getPullRequestInfo(pullrequestId) {
    try {
      const { data } = await Axios.get(
        `${this.baseApiUrl}/pullrequests/${pullrequestId}`,
        {
          headers: { Authorization: `Bearer ${this.accessToken}` },
        },
      );

      return data;
    } catch (e) {
      logger.error(e, 'getPullRequestInfo');

      return undefined;
    }
  }

  async getBranches(cardIdShort) {
    try {
      const { data } = await Axios.get(
        `${this.baseApiUrl}/refs`,
        {
          headers: { Authorization: `Bearer ${this.accessToken}` },
          params: {
            q: `name~"${cardIdShort}"`,
          },
        },
      );

      return data.values;
    } catch (e) {
      logger.error(e, 'getBranches');

      return undefined;
    }
  }
}
