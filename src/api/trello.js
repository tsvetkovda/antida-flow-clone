import Axios from 'axios';
import { logger } from '../utils/logger';

export class TrelloApi {
  constructor(key, token, appHostName) {
    this.http = Axios.create({
      baseURL: 'https://api.trello.com/1/',
      params: {
        key,
        token,
      },
    });
    this.appHostName = appHostName;
  }

  async getBoardLists(boardId) {
    try {
      const { data } = await this.http.get(`/boards/${boardId}/lists/`);

      return data;
    } catch (e) {
      logger.error(e, 'getBoardLists');
      return [];
    }
  }

  async getBoard(boardId) {
    try {
      const { data } = await this.http.get(`/boards/${boardId}/`);

      return data;
    } catch (e) {
      logger.error(e, 'getBoard');
      return undefined;
    }
  }

  async getBoardCards(boardId) {
    try {
      const { data } = await this.http.get(`/boards/${boardId}/cards/`);

      return data;
    } catch (e) {
      logger.error(e, 'getBoardCards');
      return [];
    }
  }

  setCardList(cardId, idList) {
    return this.http.put(`/cards/${cardId}/`, undefined, {
      params: { idList },
    });
  }

  async createBoardUpdateHook(project) {
    try {
      await this.http.post('/webhooks/', undefined, {
        params: {
          idModel: project.board.id,
          callbackURL: `${this.appHostName}${project.name}/hooks/board-update/`,
        },
      });
    } catch (e) {
      if (e.response && e.response.data !== 'A webhook with that callback, model, and token already exists') {
        logger.error(e, 'createBoardUpdateHook');
      }
    }
  }
}
